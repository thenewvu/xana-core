'use strict'

const createServer = require('http').createServer
const express = require('express')
const io = require('socket.io')
const pick = require('lodash.pick')
const sessionEx = require('express-session')
const hook = require('xana-hook')
const methodsOf = require('xana-methods-of')

/**
 * Return a socket.io middleware that creates a session on each socket.
 * @param opts
 */
function sessionIo (opts) {
  const setupSession = sessionEx(opts)
  return (sck, next) => {
    const res = {end () {}}
    setupSession(sck.client.request, res, next)
  }
}

/**
 * Standardize a socket.io request.
 * @param req
 * @param path
 * @param opts
 * @param sck
 * @returns {Object}
 * @private
 */
function standardizeReqIo (req, path, sck, opts) {
  const stdreq = pick(req, ['query', 'body', 'files'])
  stdreq.path = path
  stdreq.ip = sck.handshake.address
  stdreq.socket = sck
  stdreq.transport = 'socket.io'
  stdreq.rawReq = sck.client.request
  opts.session && (stdreq.session = sck.client.request.session)
  return stdreq
}

/**
 * Standardize a express.js request.
 * @param req
 * @param opts
 * @returns {Object}
 * @private
 */
function standardizeReqEx (req, opts) {
  const stdreq = pick(req, ['ip', 'query', 'body', 'files'])
  stdreq.path = req.originalUrl
  stdreq.socket = {join () {}, leave () {}}
  stdreq.transport = 'express'
  stdreq.rawReq = req
  opts.session && (stdreq.session = req.session)
  return stdreq
}

/**
 * Create a service router for socket.io.
 * @param {Service} service
 * @param opts
 * @returns {Function} A socket.io middleware.
 * @private
 */
function routeIo (service, opts) {
  const methods = methodsOf(service)
  return (sck, next) => {
    methods.forEach((method) => {
      const path = `${service.path}/${method}`
      sck.on(path, (req, done) => {
        const stdreq = standardizeReqIo(req, path, sck, opts)
        service[method].call(service, stdreq, (err, res) => {
          if (stdreq.rawReq.session && typeof stdreq.rawReq.session.save === 'function') {
            stdreq.rawReq.session.save()
          }
          if (typeof done === 'function') {
            done(err, res)
          }
        })
      })
    })

    next()
  }
}

/**
 * Create a service router for express.js.
 * @param {Service} service
 * @param opts
 * @returns {Function} An express.js middleware.
 * @private
 */
function routeEx (service, opts) {
  return (req, res) => {
    const method = service[req.path.slice(1)]
    if (typeof method === 'function') {
      const stdreq = standardizeReqEx(req, opts)
      method.call(service, stdreq, (error, result) => {
        if (!res.headersSent) {
          res.send(error ? {error} : {result}).end()
        }
      })
    } else {
      if (!res.headersSent) {
        res.status(404).end()
      }
    }
  }
}

/**
 * Application class.
 */
class Application {
  constructor (opts) {
    this._opts = opts || {}

    this._express = express()
    this._server = createServer(this._express)
    this._io = io(this._server, this._opts.socketio)

    this._service = {}

    if (this._opts.session) {
      this._express.use(sessionEx(this._opts.session))
      this._io.use(sessionIo(this._opts.session))
    }
  }

  get server () {
    return this._server
  }

  get express () {
    return this._express
  }

  get io () {
    return this._io
  }

  service (path) {
    return this._service[path]
  }

  /**
   * Serve a service.
   * @param {Service} service
   */
  serve (service) {
    if (this._service[service.path]) {
      throw new Error(`"${service.path}" is being used`)
    }
    this._service[service.path] = service
    this._express.use(service.path, routeEx(service, this._opts))
    this._io.use(routeIo(service, this._opts))
  }

  listen (...args) {
    this._server.listen(...args)
  }

  stop (callback) {
    this._server.stop(callback)
  }
}

/**
 * Service class.
 */
class Service {

  /**
   * Construct a service.
   * @param {String} path
   * @private
   */
  constructor (path) {
    this._path = path
    methodsOf(this).forEach(method => {
      this[method] = hook(this[method])
    })
  }

  /**
   * Get service path.
   * @returns {String}
   */
  get path () {
    return this._path
  }
}

module.exports = {
  Application,
  Service
}
